---
title: "links"
tags: ["garden"]
---

Mastodon

: [@higby@front-end.social]({{ author.mastodon }})

Codeberg

: [higby]({{ author.codeberg }})

Letterboxd

: [higby]({{ author.letterboxd }})

Email

: [{{ author.email}}](mailto:{{ author.email}})

PGP

: [652B 8C7D 91BC 4895](/pgp.txt)
