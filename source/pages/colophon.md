---
title: "colophon"
date: 2024-03-10
tags: ["garden"]
---

Code Hosting
: [Codeberg]({{ author.codeberg }}www/)

Site Hosting
: Netlify

Generator
: {{ eleventy.generator }}

Content
: Markdown

Font
: IBM Plex Sans
