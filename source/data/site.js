export default {
	description: "hypertext garden",
	url: "https://www.higby.io",
	timestamp: new Date(),
	navigation: [
		{
			title: "home",
			href: "/"
		},
		{
			title: "links",
			href: "/links/"
		},
		{
			title: "feed",
			href: "/feed.xml"
		}
	]
}
