export default {
	name: "Branden Higby",
	epithet: "Web Developer",
	email: "branden@higby.io",
	codeberg: "https://codeberg.org/higby/",
	mastodon: "https://front-end.social/@higby",
	letterboxd: "https://letterboxd.com/higby"
}
